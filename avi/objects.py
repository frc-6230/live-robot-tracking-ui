import pygame
import math
from shapely.geometry import Polygon
from shapely.geometry import Point

from shapely import affinity


class GameObject:

    def __init__(self, x, y, width, height, color):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect = pygame.rect.Rect(x, y, width, height)
        self.color = color

    def render(self, s):
        pygame.draw.rect(s, self.color, self.rect)

    def update(self):
        pass

class Obstacle(GameObject):
    def __init__(self, coords, color, meterToPixel):
        super().__init__(coords[0][0], coords[0][1], 0, 0, color)
        self.coords = coords

        count = 0
        for i in self.coords:
            t = (i[0]*meterToPixel,i[1]*meterToPixel)
            self.coords[count] = t
            count += 1

    def render(self, s):
        pygame.draw.polygon(s, self.color, self.coords)

    def update(self):
        super().update()

    def rotate(self, angle):
        poly2 = affinity.rotate(Polygon(self.coords), angle, 'center')

        x, y = poly2.exterior.coords.xy

        count = 0
        for i in self.coords:
            t = (x[count], y[count])
            self.coords[count] = t
            count += 1


class Robot(GameObject):

    def __init__(self, x, y, width, height, color, meterToPixel):
        super().__init__(x, y, width, height, color)

        self.coords = [(x, y), (x + width, y), (x+ width, y + height), (x, y + height)]

        self.meterToPixel = meterToPixel
        self.width = width*meterToPixel

        self.last_right_fixed_encoder = 0
        self.last_left_fixed_encoder = 0

        self.last_angle = 0

    def render(self, s):
        pygame.draw.polygon(s, self.color, self.coords)
        centerX = self.coords[0][0] + (self.coords[2][0]-self.coords[0][0])/2
        centerY = self.coords[0][1] + (self.coords[2][1]-self.coords[0][1])/2
        pygame.draw.line(s, (0, 255, 0), (centerX, centerY), (centerX + math.cos(math.radians(self.last_angle)) * 50, centerY+ 50 * math.sin(math.radians(self.last_angle))))

    def update(self, w, left, right, a, obsarr):
        super().update()
        r = left
        l = right
        width = w
        angle = a

        fix = 0

        # if angle != 0:
        #     fix = ((width / 2) * math.pi) / (360 / angle)

    #     l -= fix
    #     r += fix
    #
        delta_right_fix = r - self.last_right_fixed_encoder
        delta_left_fix = l - self.last_left_fixed_encoder

        distance = ((delta_left_fix + delta_right_fix)/2) * self.meterToPixel

        addX = distance * math.cos(math.radians(angle))
        addY = distance * math.sin(math.radians(angle))

        self.last_left_fixed_encoder = l
        self.last_right_fixed_encoder = r
        self.last_angle = angle

        for obs in obsarr:
            if self.collides(obs, addX, addY):
                return

        count = 0
        for i in self.coords:
            t = (i[0] + addX, i[1] + addY)
            self.coords[count] = t
            count += 1

    def contains(self,x,y):
        p = Point(x,y)
        poly = Polygon(self.coords)
        return poly.contains(p)

    def move(self,side, obsarr):
        x, y = 0, 0
        if side == "up":
            y-=5
        if side == "right":
            x+=5
        if side == "down":
            y+=5
        if side == "left":
            x-=5

        for obs in obsarr:
            if self.collides(obs, x, y):
                return

        count = 0
        for i in self.coords:
            t = (i[0] + x, i[1] + y)
            self.coords[count] = t
            count += 1
        # self.x += x
        # self.y += y
        #

    def updateXY(self,x, y):

        deltaX = x - self.coords[0][0]
        deltaY = y - self.coords[0][1]
        count = 0
        for i in self.coords:
            t = (i[0] + deltaX, i[1] + deltaY)
            self.coords[count] = t
            count += 1


    def collides(self, obs, x, y):
        coords = []
        count = 0
        for i in self.coords:
            t = (i[0] + x, i[1] + y)
            coords.append(t)
            count += 1
        p1 = Polygon(coords)
        p2 = Polygon(obs.coords)
        return p1.intersects(p2)

    def rotate(self, angle):
        poly2 = affinity.rotate(Polygon(self.coords), angle, 'center')

        x, y = poly2.exterior.coords.xy

        count = 0
        for i in self.coords:
            t = (x[count], y[count])
            self.coords[count] = t
            count += 1




class ObstacleAdder():

    def __init__(self, meterToPixel):
        self.points = []
        self.pointsMeter = []
        self.finished = False
        self.pointR = 6
        self.meterToPixel = meterToPixel


    def addPoint(self, x, y):

        if self.finished:
            return


        if len(self.points) > 3 and math.sqrt((x - self.points[0][0])**2 + (y - self.points[0][1])**2) <= 10:
                self.finished = True
        else:
            self.points.append((x, y))
            self.pointsMeter.append((x/self.meterToPixel, y/self.meterToPixel))


    def getResult(self):
        return self.points

    def render(self, s):
        for c in self.points:
            pygame.draw.circle(s, (255, 0, 255), c, self.pointR)
