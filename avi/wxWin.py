import wx
import json

WIN_WIDTH = 400
WIN_HEIGHT = 300

FONT = None
dc = None

OFFSET = 3

RED = wx.Colour(255, 0, 0)
BLUE = wx.Colour(0, 0,255)
GREEN = wx.Colour(0, 255, 0)
PURPLE = wx.Colour(255, 0, 255)
DEFAULT_COLOR = wx.Colour(240, 240, 240, 255)


class SetupFrame(wx.Frame):
    def __init__(self):

        app = wx.App(False)

        super().__init__(parent=None, title='Field Setup', style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER, size=(WIN_WIDTH, WIN_HEIGHT))

        global FONT, dc

        FONT = wx.Font(pointSize=12, family=wx.DEFAULT,
                       style=wx.NORMAL, weight=wx.NORMAL,
                       faceName='Consolas')
        dc = wx.ScreenDC()
        dc.SetFont(FONT)

        self.SetMaxSize(wx.Size(WIN_WIDTH, WIN_HEIGHT))
        self.SetMinSize(wx.Size(WIN_WIDTH, WIN_HEIGHT))

        self.panel = wx.Panel(self)

        self.fieldWidthBar = EditText(10, 10, "Field Width (m) :", self.panel)

        self.fieldHeightBar = EditText(10, self.fieldWidthBar.getHeight() + self.fieldWidthBar.editText.Rect[1] + OFFSET, "Field Height (m) :", self.panel)

        self.robotWidthBar = EditText(10, self.fieldHeightBar.getHeight() + self.fieldHeightBar.editText.Rect[1] + OFFSET, "Robot Width (m) :", self.panel)

        self.robotLengthBar = EditText(10, self.robotWidthBar.getHeight() + self.robotWidthBar.editText.Rect[1] + OFFSET, "Robot Length (m) :", self.panel)

        startButton = wx.Button(self.panel, label='Setup Field', pos=(5, 110))

        startButton.Bind(wx.EVT_BUTTON, self.setup_field_press)

        loadButton = wx.Button(self.panel, label='Load Field', pos=(5, startButton.Rect[1] + startButton.Rect[3] + OFFSET))
        loadButton.Bind(wx.EVT_BUTTON, self.load_file_press)



        self.data = {}

        self.fieldWidth = None
        self.fieldHeight = None
        self.robotWidth = None
        self.robotLength = None


        self.Show()

        app.MainLoop()

    def setup_field_press(self,event):

        try:
            self.data["fieldWidth"] = float(self.fieldWidthBar.editText.GetValue())
            self.data["fieldHeight"] = float(self.fieldHeightBar.editText.GetValue())
            self.data["robotWidth"] = float(self.robotWidthBar.editText.GetValue())
            self.data["robotLength"] = float(self.robotLengthBar.editText.GetValue())

            self.Destroy()

        except:
            print("invalid values")

    def load_file_press(self,event):
        with wx.FileDialog(self, "Load JSON file", wildcard="JSON files (*.json)|*.json",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # save the current contents in the file
            pathname = fileDialog.GetPath()
            try:
                with open(pathname) as json_file:
                    self.data = json.load(json_file)

                    self.Destroy()
            except IOError:
                wx.LogError("Cannot save current data in file '%s'." % pathname)



class FieldUtilitiesFrame(wx.Frame):

    def __init__(self):

        self.app = wx.App(False)

        super().__init__(parent=None, title='Field Setup', style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER, size=(WIN_WIDTH, WIN_HEIGHT))

        global FONT, dc

        FONT = wx.Font(pointSize=12, family=wx.DEFAULT,
                       style=wx.NORMAL, weight=wx.NORMAL,
                       faceName='Consolas')
        dc = wx.ScreenDC()
        dc.SetFont(FONT)

        self.panel = wx.Panel(self)
        self.panel.SetFont(FONT)

        self.obstacleButton = wx.Button(self.panel, label='Add Obstacle', pos=(5, 40))
        self.obstacleButton.SetPosition(((WIN_WIDTH - self.obstacleButton.Rect[2])/2, 40))
        self.obstacleButton.Bind(wx.EVT_BUTTON, self.obstacle_button_press)

        self.saveSettingsButton = wx.Button(self.panel, label='Save Settings', pos=(5, 90))
        self.saveSettingsButton.SetPosition(((WIN_WIDTH - self.saveSettingsButton.Rect[2]) / 2,
                                         self.obstacleButton.Rect[1] + self.obstacleButton.Rect[3] + OFFSET))
        self.saveSettingsButton.Bind(wx.EVT_BUTTON, self.save_settings_press)

        self.loadIMGButton = wx.Button(self.panel, label='Load Image', pos=(5, 90))
        self.loadIMGButton.SetPosition(((WIN_WIDTH - self.loadIMGButton.Rect[2]) / 2,
                                         self.saveSettingsButton.Rect[1] + self.saveSettingsButton.Rect[3] + OFFSET))
        self.loadIMGButton.Bind(wx.EVT_BUTTON, self.load_img_file_press)

        self.obstacles_checkbox = wx.CheckBox(self.panel, label='Show obstacles', pos=(10, 200))

        self.pathname = None
        self.imageName= None

        self.mode = None

        self.Show()

    def mainLoop(self):
        self.app.MainLoop()


    def obstacle_button_press(self, event):
        if self.mode == "obstacle":
            self.disable_all_buttons()
            return

        self.disable_all_buttons()
        self.obstacleButton.SetBackgroundColour(PURPLE)
        self.mode = "obstacle"


    def save_settings_press(self,event):
        with wx.FileDialog(self, "Save to JSON file", wildcard="JSON files (*.json)|*.json",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # save the current contents in the file
            self.pathname = fileDialog.GetPath()
            # try:
            #     with open(pathname, 'w') as file:
            #         self.doSaveData(file)
            # except IOError:

        if self.mode == "save":
            self.disable_all_buttons()
            return

        self.disable_all_buttons()
        self.mode = "save"

    def load_img_file_press(self, event):
        with wx.FileDialog(self, "Load Image", wildcard="PNG files (*.png)|*.png",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # save the current contents in the file
            self.imageName = fileDialog.GetPath()

        if self.mode == "img":
            self.disable_all_buttons()
            return

        self.disable_all_buttons()
        self.mode = "img"


    def disable_all_buttons(self):
        self.obstacleButton.SetBackgroundColour(DEFAULT_COLOR)
        self.mode = None


class EditText:

    def __init__(self, x, y, text, panel):

        self.text = text

        self.textView = wx.StaticText(panel, -1, text, pos=(x, y))
        self.textView.SetFont(FONT)

        self.editText = wx.TextCtrl(panel, pos=(dc.GetTextExtent(text)[0] + self.textView.Rect[0] + OFFSET, self.textView.Rect[1] - OFFSET / 2))

        self.setEditTextWidth(40)



    def setEditTextWidth(self, width):
        self.editText.SetSize(width, dc.GetTextExtent(self.text)[1] + OFFSET)


    def getWidth(self):
        return dc.GetTextExtent(self.text)[0] + OFFSET


    def getHeight(self):
        return dc.GetTextExtent(self.text)[1] + OFFSET

