from avi.objects import *
from avi.wxWin import *
import json


frame = SetupFrame()

data = frame.data
pygame.init()

pcw, pch = pygame.display.Info().current_w, pygame.display.Info().current_h


meterToPixel = 0

optimalw = pcw * (2/3)
optimalh = pch *(8/9)

if ((optimalw/data["fieldWidth"]) * data["fieldHeight"]) > optimalh:
    meterToPixel = optimalh / data["fieldHeight"]
else:
    meterToPixel = optimalw / data["fieldWidth"]

size = WIDTH, HEIGHT = int(data["fieldWidth"] * meterToPixel), int(data["fieldHeight"] * meterToPixel)


utilityFrame = FieldUtilitiesFrame()
obstacles = []

if data.keys().__contains__("obstacles"):
    for arr in data["obstacles"]:
        obstacles.append(Obstacle(arr.copy(), (255, 0, 255), meterToPixel))
else:
    data["obstacles"] = []

if data.keys().__contains__("show_obstacles"):
    utilityFrame.obstacles_checkbox.SetValue(data["show_obstacles"])
else:
    utilityFrame.obstacles_checkbox.SetValue(True)
    data["show_obstacles"] = True




def main():



    img = None

    try:
        img = pygame.image.load(data["image"])
        img = pygame.transform.scale(img, (WIDTH, HEIGHT))
    except:
        pass

    print("Connecting to 6230")
    '''
    connection
    '''


    #Init
    clickPos = (0, 0)

    dragPos = (0, 0)
    offsetX = 0
    offsetY = 0
    drag = False

    screen = pygame.display.set_mode(size)
    robot = Robot(300, 150, int(data["robotLength"]*meterToPixel), int(data["robotWidth"]*meterToPixel), (255, 0, 0), meterToPixel)

    obstacle_adder = None


    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((255, 255, 255))

    angle = 0
    l = 0
    r = 0


    #Game loop
    running = True
    clock = pygame.time.Clock()
    while running:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONUP:
                clickPos = pygame.mouse.get_pos()
                drag = False

                if utilityFrame.mode == "obstacle":

                    if not obstacle_adder:
                        obstacle_adder = ObstacleAdder(meterToPixel)

                    obstacle_adder.addPoint(clickPos[0], clickPos[1])

                    if obstacle_adder.finished:
                        obstacles.append(Obstacle(obstacle_adder.pointsMeter.copy(),(255, 0, 255), meterToPixel))
                        data['obstacles'].append(obstacle_adder.pointsMeter.copy())

                        obstacle_adder = None

                        utilityFrame.disable_all_buttons()

            if event.type == pygame.MOUSEBUTTONDOWN:
                clickPos = pygame.mouse.get_pos()

                if not drag and robot.contains(clickPos[0], clickPos[1]):
                    drag = True
                    offsetX = clickPos[0] - robot.coords[0][0]
                    offsetY = clickPos[1] - robot.coords[0][1]
            if event.type == pygame.MOUSEMOTION:
                dragPos = pygame.mouse.get_pos()

                if drag:
                    robot.updateXY(dragPos[0]- offsetX, dragPos[1] - offsetY)




        if utilityFrame.mode != "obstacle":
            obstacle_adder = None

        if utilityFrame.mode == "save":
            data["show_obstacles"] = utilityFrame.obstacles_checkbox.GetValue()
            with open(utilityFrame.pathname, 'w') as outfile:
                json.dump(data, outfile)
            utilityFrame.disable_all_buttons()

        if utilityFrame.mode == "img":
            img = pygame.image.load(utilityFrame.imageName)
            img = pygame.transform.scale(img, (WIDTH, HEIGHT))
            data["image"] = utilityFrame.imageName
            utilityFrame.disable_all_buttons()

        screen.blit(background, (0, 0))


        if img:
            screen.blit(img, (0, 0))

        if utilityFrame.obstacles_checkbox.GetValue():
            for o in obstacles:
                o.render(screen)

        keys = pygame.key.get_pressed()

        if keys[pygame.K_w]:
            l+= 0.1
            r+=0.1
        if keys[pygame.K_s]:
            l -=0.1
            r-=0.1

        if keys[pygame.K_RIGHT]:
            robot.rotate(3)
            angle +=3
        if keys[pygame.K_LEFT]:
            robot.rotate(-3)
            angle -=3

        robot.update(data["robotWidth"],l,r,angle,obstacles)
        robot.render(screen)


        if obstacle_adder:
            obstacle_adder.render(screen)

        pygame.display.update()
        clock.tick(60)



if __name__ == '__main__':
    main()
